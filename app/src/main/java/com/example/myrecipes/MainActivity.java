package com.example.myrecipes;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

public class MainActivity extends AppCompatActivity implements AboutUsFragment.OnFragmentInteractionListener,RecipesFragment.OnFragmentInteractionListener,DetailFragment.OnFragmentInteractionListener {

    private DrawerLayout drawerLayout;
    private FrameLayout containerFrameLayout;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        drawerLayout = findViewById(R.id.drawer_layout);

        NavigationView navigationView = findViewById(R.id.navigation_view);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                int item = menuItem.getItemId();
                itemSelected(item);

                return true;
            }
        });


        containerFrameLayout =findViewById(R.id.container);

    }

    private void itemSelected(int itemId) {
        switch (itemId) {
            case R.id.recipes_item:
                containerFrameLayout.setVisibility(View.VISIBLE);
                FragmentTransaction transactionRecipes = getFragmentManager().beginTransaction();
                transactionRecipes.replace(R.id.container, new RecipesFragment());
                transactionRecipes.commit();

                break;
            case R.id.aboutus_item:
                containerFrameLayout.setVisibility(View.VISIBLE);
                FragmentTransaction transactionAboutus = getFragmentManager().beginTransaction();
                transactionAboutus.replace(R.id.container, new AboutUsFragment());
                transactionAboutus.commit();
                break;
            default:
                containerFrameLayout.setVisibility(View.INVISIBLE);
                break;
        }

        drawerLayout.closeDrawers();
    }

    @Override
    public void onBackPressed() {

        if(containerFrameLayout.getVisibility() == View.VISIBLE){
            containerFrameLayout.setVisibility(View.INVISIBLE);
        }else{
            super.onBackPressed();
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }


    @Override
    public void openDescription(Recipe recipe) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.container, DetailFragment.newInstance(recipe));
        transaction.commit();
    }

    @Override
    public void onDetailFragmentBack() {
        containerFrameLayout.setVisibility(View.INVISIBLE);
    }
}
