package com.example.myrecipes;

import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;

public class Recipe implements Serializable {

    private String title;
    private int photo;
    private List<String> ingredient;
    private String instructions;


    public Recipe(String title, int photo, List<String> ingredient, String instructions) {
        this.title = title;
        this.photo = photo;
        this.ingredient = ingredient;
        this.instructions = instructions;
    }

    public String getTitle() {
        return title;
    }

    public int getPhoto() {
        return photo;
    }

    public List<String> getIngredient() {
        return ingredient;
    }

    public String getInstructions() {
        return instructions;
    }

}
