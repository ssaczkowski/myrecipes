package com.example.myrecipes;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;


public class DetailFragment extends Fragment {
    public static final String ARG_RECIPE = "currentRecipe";

    private static Recipe mRecipeSelected;

    private OnFragmentInteractionListener mListener;

    public DetailFragment() {
        // Required empty public constructor
    }


    public static DetailFragment newInstance(Recipe recipe) {
        DetailFragment fragment = new DetailFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_RECIPE, recipe);
        fragment.setArguments(args);

        mRecipeSelected = (Recipe) args.getSerializable(ARG_RECIPE);


        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mRecipeSelected = (Recipe) getArguments().getSerializable(ARG_RECIPE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail, container, false);

        ScrollView scrollView = view.findViewById(R.id.scrollView);

        LinearLayout container_detail = view.findViewById(R.id.container_detail);

        ImageView imageView = view.findViewById(R.id.image_recipe);
        imageView.setImageResource(mRecipeSelected.getPhoto());

        TextView title_recipe = view.findViewById(R.id.title_recipe);
        title_recipe.setText(mRecipeSelected.getTitle());

        TextView ingredients_recipe = view.findViewById(R.id.ingredients_recipe);
        ingredients_recipe.setText(mRecipeSelected.getIngredient().toString());

        TextView instruction_recipe = view.findViewById(R.id.instruction_recipe);
        instruction_recipe.setText(mRecipeSelected.getInstructions());


        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        void onDetailFragmentBack();
    }


}
