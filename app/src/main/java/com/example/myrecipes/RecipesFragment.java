package com.example.myrecipes;

import android.support.v7.widget.SearchView;
import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.NonNull;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;


public class RecipesFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private RecipeAdapter recipeAdapter;
    private List<Recipe> recipesList = getRecipesHardcode();

    public RecipesFragment() {
        // Required empty public constructor
    }

    public static RecipesFragment newInstance() {
        RecipesFragment fragment = new RecipesFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_recipes, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);

        recyclerView.setLayoutManager(layoutManager);

        recipeAdapter = new RecipeAdapter(recipesList, mListener);
        recyclerView.setAdapter(recipeAdapter);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP|ItemTouchHelper.DOWN ,ItemTouchHelper.LEFT|ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder dragged, @NonNull RecyclerView.ViewHolder target) {

                int positionDragged = dragged.getAdapterPosition();
                int positionTarget = target.getAdapterPosition();

                Collections.swap(recipesList,positionDragged,positionTarget);

                recipeAdapter.notifyItemMoved(positionDragged,positionTarget);

                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder target, int i) {

                int position = target.getAdapterPosition();
                recipesList.remove(position);
                recipeAdapter.notifyDataSetChanged();

            }
        });
        itemTouchHelper.attachToRecyclerView(recyclerView);

        return view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.search_menu,menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView   = (SearchView) searchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                recipeAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String texto) {
                recipeAdapter.getFilter().filter(texto);
                return false;
            }
        });

        super.onCreateOptionsMenu(menu, inflater);
    }


    private List<Recipe> getRecipesHardcode() {
        ArrayList<Recipe> recipes = new ArrayList<>();

        recipes.add(getRecipe2());
        recipes.add(getRecipe1());
        recipes.add(getRecipe2());
        recipes.add(getRecipe1());
        recipes.add(getRecipe2());
        recipes.add(getRecipe1());
        recipes.add(getRecipe2());
        recipes.add(getRecipe1());
        recipes.add(getRecipe2());
        recipes.add(getRecipe1());
        recipes.add(getRecipe2());
        recipes.add(getRecipe1());
        recipes.add(getRecipe2());
        recipes.add(getRecipe1());

        return recipes;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        void openDescription(Recipe recipe);
    }



    public class RecipeAdapter extends RecyclerView.Adapter implements Filterable {

        private List<Recipe> recipeList;
        private List<Recipe> recipeListComplete;
        private OnFragmentInteractionListener listener;

        public RecipeAdapter(List<Recipe> recipeList, OnFragmentInteractionListener listener) {
            this.recipeList = recipeList;
            this.listener = listener;
            this.recipeListComplete = new ArrayList<>(recipeList);
        }


        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

            LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
            View view = inflater.inflate(R.layout.recipe_item, viewGroup,false);
            final RecipeViewHolder recipeViewHolder = new RecipeViewHolder(view);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.openDescription(recipeList.get(recipeViewHolder.getAdapterPosition()));
                }
            });

            return recipeViewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

            Recipe currentRecipe = this.recipeList.get(i);
            RecipeViewHolder recipeViewHolder = (RecipeViewHolder) viewHolder;

            recipeViewHolder.bindRecipe(currentRecipe);

        }

        @Override
        public int getItemCount() {
            return this.recipeList.size();
        }

        @Override
        public Filter getFilter() {
            return recipeFilter;
        }

        private Filter recipeFilter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                List<Recipe> recipeListFiltered = new ArrayList<>();
                if (constraint == null || constraint.length() == 0 ){
                    recipeListFiltered.addAll(recipeListComplete);
                }else{
                    String pattern = constraint.toString().toLowerCase().trim();
                    for (Recipe item : recipeListComplete){
                        if(item.getTitle().toLowerCase().contains(pattern)){
                            recipeListFiltered.add(item);
                        }
                    }
                }

                FilterResults results = new FilterResults();
                results.values = recipeListFiltered;

                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                recipeList.clear();
                recipeList.addAll((List) results.values);
                notifyDataSetChanged();
            }
        };

    }

    private class RecipeViewHolder extends RecyclerView.ViewHolder{
        private ImageView image;
        private TextView title;
        private Recipe recipe;

        public RecipeViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            title = itemView.findViewById(R.id.title);
        }

        public void bindRecipe(Recipe recipe) {
            this.recipe = recipe;
            this.title.setText(this.recipe.getTitle());
            this.image.setImageResource(this.recipe.getPhoto());
        }
    }


    //HARDCODE

    private Recipe getRecipe2() {
        ArrayList<String> ingredients2 = new ArrayList<>();
        ingredients2.add("6 yemas de huevo");
        ingredients2.add("20 cucharadas de azúcar");
        ingredients2.add("8 cucharadas de oporto");
        ingredients2.add("250cm cúbicos de crema");
        ingredients2.add("2 claras de huevo");
        ingredients2.add("1 cucharada de fécula de maíz");
        return new Recipe("Helado Sambayón",R.drawable.icecream_sambayon,ingredients2,"Cocinar todos estos ingredientes hasta que comiencen a hervir, mezclando continuamente para evitar que se formen grumos, o que la preparación se queme.\n" +
                "\n" +
                "Una vez llevada al punto de hervor, cocinar durante dos minutos y retirar del fuego.\n" +
                "Reservar esta mezcla con un film en contacto para que no se forme en la superficie una película seca.\n" +
                "Por otro lado, batir la crema sin llegar a un punto muy sostenido (ya que después hay que integrarla a la preparación, y un exceso de batido la cortaría)\n" +
                "\n" +
                "En otro bol, montar las claras a punto nieve y reservar.\n" +
                "Una vez que la mezcla de huevos y Oporto haya enfriado, agregarle las claras y la crema batida con movimientos envolventes hasta lograr una preparación homogénea.\n" +
                "Llevar al freezer por al menos cinco horas.\n" +
                "\n" +
                "Como siempre que se prepara helado casero, es conveniente revolver de cuando en cuando el helado mientras se va enfriando, para que no se cristalice.\n" +
                "\n" +
                "Una vez llegado a  su punto,  el helado de sambayón estará listo para disfrutar y  compartir.\n" +
                "\n" +
                "Como toque final se le pueden agregar nueces, almendras tostadas, o pedacitos de chocolate. Servirlo con hilos de caramelo, con salsa de cacao, o en copas con trocitos de merengue.");

    }

    private Recipe getRecipe1() {
        ArrayList<String> ingredients1 = new ArrayList<>();
        ingredients1.add("3 tazas de harina (420 gramos)");
        ingredients1.add("3 tazas de azúcar (600 gramos)");
        ingredients1.add("2½ cucharaditas de bicarbonato");
        ingredients1.add("1 cucharadita de polvos de hornear o levadura tipo Royal");
        ingredients1.add("1 pizca de sal");
        ingredients1.add("1 pizca de canela en polvo");
        ingredients1.add("1½ tazas de cocoa o cacao en polvo");
        ingredients1.add("1½ tazas de aceite");
        return new Recipe("Torta de Chocolate",R.drawable.chocolate_cake,ingredients1,"Para hacer esta torta húmeda de chocolate decorada, cogemos un recipiente grande y tamizamos la harina junto con la cocoa, el bicarbonato y el polvo de hornear." +
                " Añadimos dos tazas de azúcar, la sal y la canela y mezclamos bien. Separamos las claras de las yemas de huevo. Introducimos a la mezcla anterior el aceite y la leche y mezclamos bien. Luego, para seguir con la preparación de la torta de chocolate húmeda y esponjosa, añadimos el vinagre y seguimos batiendo. " +
                "Agregamos ahora las yemas de huevo y la esencia de vainilla y continuamos mezclando. Aparte, batimos las claras a punto de nieve con el azúcar restante y las incorporamos a la mezcla con la ayuda de una espátula, haciendo movimientos envolventes suaves. " +
                "Vertemos la masa del bizcocho húmedo de chocolate en un molde para horno previamente enharinado y enmantecado y lo horneamos a 180 ºC (con el horno previamente precalentado) durante 50 minutos. " +
                "Retiramos la torta de chocolate húmeda del horno, la dejamos enfriar sobre una rejilla a temperatura ambiente y después la desmoldamos.");
    }

}
